
$(window).on('load', function() {
	$('.js-pre-load').addClass('hide');
	$('.js-post-load').addClass('show');
});




var canvas = document.querySelector('canvas');
// var canvas = $(document).find('canvas');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext('2d');

// Rectangles 
// x, y, width, height
// c.fillStyle = 'rgba(255, 0, 0, 0.5)';
// c.fillRect(100, 100, 100, 100);
// c.fillStyle = 'rgba(0, 0, 255, 0.5)';
// c.fillRect(400, 100, 100, 100);
// c.fillStyle = 'rgba(0, 255, 0, 0.5)';
// c.fillRect(300, 300, 100, 100);


// Line
// x, y
// c.beginPath();
// c.moveTo(50, 300);
// c.lineTo(300, 100);
// c.lineTo(400, 300);
// c.strokeStyle = "#ddd";
// c.stroke();


// Arc / Circle
// values: x, y, radius, startAngle, endAngle, drawCounterClockwise(bool)
// c.beginPath();
// c.arc(300, 300, 30, 0, Math.PI * 2, false);
// c.strokeStyle = 'blue';
// c.stroke();


// Randomize
// for(var i = 0; i < 20; i++) {
// 	// Math.random returns any number from 0 - 1
// 	// Multiplying it increases max values it can be
// 	var x = Math.random() * window.innerWidth,
// 		y = Math.random() * window.innerHeight;
// 	c.beginPath();
// 	c.arc(x, y, 30, 0, Math.PI * 2, false);
// 	c.strokeStyle = 'blue';
// 	c.stroke();
// }


var x = 200,
	y = 200,
	dy = 4,
	radius = 30,
	dx = 7;


// Animating
function animate() {
	requestAnimationFrame(animate);
	// Clear entire canvas from x to y, width to innerHeight
	c.clearRect(0, 0, innerWidth, innerHeight);
	
	c.beginPath();
	c.arc(x, y, radius, 0, Math.PI * 2, false);
	c.strokeStyle = 'blue';
	c.stroke();

	// Speed/Direction
	if(x + radius > window.innerWidth || x - radius < 0){
		dx = -dx;
	}

	if(y + radius > window.innerHeight || y - radius < 0){
		dy = -dy;
	}

	x += dx;
	y += dy;
}

animate();


